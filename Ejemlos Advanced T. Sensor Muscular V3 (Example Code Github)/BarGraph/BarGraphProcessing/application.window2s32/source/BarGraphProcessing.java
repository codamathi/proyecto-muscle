import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.serial.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class BarGraphProcessing extends PApplet {

/*
*  By Hannah Perner-Wilson, www.plusea.at
*  Modified 3/30/2012 By Brian E Kaminski, www.AdvancerTechnologies.com
*
*  IMPORTANT!!:
*  Scroll down to set your serial port
*  Scroll down to set your thresholds
*/



// definition of window size
// you can change the size of the window as you like
// the thresholdGraph will be scaled to fit
// the optimal size for the thresholdGraph is 1000 x 400

int xWidth = 1000;
int yHeight = 625;

// xPos input array, using prefix
int[] xPosArr= {0,0,0,0,0,0}; 

// 
int[] messageArr= {0,0,0,0,0,0}; 
  
// Arrays for threshholding
int[] threshMax= {0,0,0,0,0,0}; 
int[] threshMin= {0,0,0,0,0,0}; 
  
// variables for serial connection. portname and baudrate are user specific
Serial port1;

//Set your serial port here (look at list printed when you run the application once)
String V3 = Serial.list()[0];
String portname1 = V3;
int baudrate = 115200; //MODIFICAR SI TOCAS BAUDIOS
  
int prefix = 10001;
boolean myCatch = false;
String serialIN = "";
String serialINPUT = ""; 
String buffer = ""; 
int value = 0; 

// ThresholdGraph draws grid and poti states
ThresholdGraph in;

public void setup(){
  // set size and framerate
  
  frameRate(25);
  background(255);
  strokeWeight(5);
  stroke(0);
  
  strokeCap(ROUND);

  // establish serial port connection      
  port1 = new Serial(this, portname1, baudrate);
  port1.bufferUntil('\n') ;
  
    println(Serial.list());  // print serial list

  // create DisplayItems object
  in = new ThresholdGraph();
  
  // THRESHOLD VALUES:
  // using the thresholdGraph you can determine the MIN and MAX values
  // of your sensors. Enter these here. They must lie between 0 and 1000.
  
    //MIN trashhold
    threshMin[0] = 20;   // one
    threshMin[1] = 20;   // two
    threshMin[2] = 20;   // three
    threshMin[3] = 20;   // four
    threshMin[4] = 20;   // five
    threshMin[5] = 20;   // six
    
    //MAX trashhold
    threshMax[0] = 900;   // one
    threshMax[1] = 900;   // two
    threshMax[2] = 900;   // three
    threshMax[3] = 900;   // four
    threshMax[4] = 900;   // five
    threshMax[5] = 900;   // six
}//end setup

// draw listens to serial port, draw 
public void draw(){
  
  // listen to serial port and trigger serial event  
  while(port1.available() > 0){
        serialIN = port1.readStringUntil('\n');
        //println((int)float(serialIN));
        if(serialIN != "")
          serialEvent(serialIN);  
        else
          println("found empty");
        }
        
  // threshold serial input  
  threshHolding();  

  // draw serial input
  in.update();
}//end draw()
public void serialEvent(String serialINPUT) {
  try
  {
    int inByte = Integer.parseInt(serialINPUT.replaceAll("(\\r|\\n)", ""));

    if (myCatch == true) {
      if (inByte != 10010) {        
        // add event to buffer
        value = inByte; 
        xPosArr[(prefix-10001)] = value;
        //println("found data");
        println(value);
      }
      else {
        // if serial is line break set value to buffer value and clear buffer
        myCatch = false;     
        //println("found footer");
      }
    } 
    //myCatch is the beginging of the each sended number used to define the port
    // myCatch gets true if serial is less than 10010 since header is 10001-10009
    if (!myCatch && inByte < 10010 && inByte > 10000) { 
      myCatch = true;
      prefix = inByte;  
      //println("found header");  
      //println(prefix);
    }
  }
  catch(NumberFormatException e)
  {
    //println(e.getMessage());
  }
  catch(NullPointerException e)
  {
    //println(e.getMessage());
  }
}
public void threshHolding(){
  for (int i=0; i<threshMin.length; i++) {
    float maxi=threshMax[i];
    float mini=threshMin[i];
    float dif = (maxi-mini)/255;
    messageArr[i]= xPosArr[i]-threshMin[i];
    if (messageArr[i] <0) messageArr[i]=0;
    float temp = messageArr[i];   
    temp = temp/dif;
    messageArr[i] = PApplet.parseInt(temp);
    if (messageArr[i] <0) messageArr[i]=0;
    if (messageArr[i] >255) messageArr[i]=255;
    }
}
/*
*  
*  This class draws a grid with a value scale and 
*  color rectangles acording to sensor input.
*
*/

class ThresholdGraph{

// variables of DisplayItems object 
PFont font;
int fontsize = 12;
String fontname = "Monaco-14.vlw";
String inputName = "";
float gridSpacing = width/42.0f;
int gridValues = 25;
float potiHeight = height/6.0f;
int potiWidth = 30;
int poti_ID = 0;
float scaleValue = width/1050.0f;
//float scaleValueY = height/400.0;

// constructor
ThresholdGraph(){
  font = loadFont(fontname);
  textFont(font, fontsize);
  }//end ThresholdGraph
 
  // draws grid and value scale
  public void update(){
    //refresh background
    background(255);
    //scale(scaleValueX, scaleValueY);  //scale everything to fit window size
    
    for (int i = 0; i < xPosArr.length; i++){
      // draw thresholds in light(min) and dark(max) grey
      noStroke();
      fill(150);
      rect(threshMin[i]*scaleValue-potiWidth/4, i*potiHeight, potiWidth/2, potiHeight);
      fill(100);
      rect(threshMax[i]*scaleValue-potiWidth/4, i*potiHeight, potiWidth/2, potiHeight); 

      // poti colours and names
      if (i == 0){ fill(255, 107, 39); inputName = "one";}
      if (i == 1){ fill(29, 224, 109); inputName = "two";}
      if (i == 2){ fill(29, 141, 224); inputName = "three";}
      if (i == 3){ fill(211, 82, 232); inputName = "four";}
      if (i == 4){ fill(100, 141, 224); inputName = "five";}
      if (i == 5){ fill(2, 82, 232); inputName = "six";}
      
      // draw poti at xpos 
      rect(xPosArr[i]*scaleValue-potiWidth/2, i*potiHeight, potiWidth, potiHeight);
      
      // draw xpos as text
      fill(0);
      text(xPosArr[i]+inputName, xPosArr[i]*scaleValue, i*potiHeight+potiHeight/2);
      }//end for
    
    // draw grid to fit window size
    stroke(0);
    strokeWeight(1);
    
    // vertical lines
    for (int i=0; i<width/gridSpacing; i++){
      line(i*gridSpacing, 0, i*gridSpacing, height);
      textAlign(LEFT);
      text(i*gridValues, i*gridSpacing+2, fontsize);
      }//end for

  }// end update
}// end class Display
  public void settings() {  size(1000, 625);  smooth(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--stop-color=#cccccc", "BarGraphProcessing" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
