/* Sweep
 by BARRAGAN <http://barraganstudio.com>
 This example code is in the public domain.

 modified 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Sweep
*/

#include <Servo.h>

Servo unoservo;  // create servo object to control a servo
Servo dosservo;
Servo tresservo;
Servo cuatroservo;
Servo cincoservo;

// twelve servo objects can be created on most boards

int uno=3;
int dos=4;
int tres=5;
int cuatro=6;
int cinco=7;

int pos =0 ;
int initpos = 180;    // variable to store the servo position
int finpos =80;

void setup() {
  unoservo.attach(uno);  // attaches the servo on pin 9 to the servo object
  dosservo.attach(dos);  // attaches the servo on pin 9 to the servo object
  tresservo.attach(tres);  // attaches the servo on pin 9 to the servo object
  cuatroservo.attach(cuatro);  // attaches the servo on pin 9 to the servo object
  cincoservo.attach(cinco);  // attaches the servo on pin 9 to the servo object
}


void dedoPulgar(){
  for (pos = initpos; pos >= finpos; pos -= 1) { // goes from 180 degrees to 0 degrees
    unoservo.write(pos);              // tell servo to go to position in variable 'pos'
        delay(15);                       // waits 15ms for the servo to reach the position
  }
  for (pos = finpos; pos <= initpos; pos += 1) { // goes from 0 degrees to 180 degrees
   unoservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
}

void dedoMenique(){
  for (pos = initpos; pos >= finpos; pos -= 1) { // goes from 180 degrees to 0 degrees
    cincoservo.write(pos);              // tell servo to go to position in variable 'pos'
        delay(15);                       // waits 15ms for the servo to reach the position
  }
  for (pos = finpos; pos <= initpos; pos += 1) { // goes from 0 degrees to 180 degrees
      cincoservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
}


void dedoAnular(){
  for (pos = initpos; pos >= finpos; pos -= 1) { // goes from 180 degrees to 0 degrees
    cuatroservo.write(pos);              // tell servo to go to position in variable 'pos'
        delay(15);                       // waits 15ms for the servo to reach the position
  }
  for (pos = finpos; pos <= initpos; pos += 1) { // goes from 0 degrees to 180 degrees
      cuatroservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
}


void dedoMedio(){
  for (pos = initpos; pos >= finpos; pos -= 1) { // goes from 180 degrees to 0 degrees
    tresservo.write(pos);              // tell servo to go to position in variable 'pos'
        delay(15);                       // waits 15ms for the servo to reach the position
  }
  for (pos = finpos; pos <= initpos; pos += 1) { // goes from 0 degrees to 180 degrees
      tresservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
}



void dedoIndice(){
  for (pos = initpos; pos >= finpos; pos -= 1) { // goes from 180 degrees to 0 degrees
    dosservo.write(pos);              // tell servo to go to position in variable 'pos'
        delay(15);                       // waits 15ms for the servo to reach the position
  }
  for (pos = finpos; pos <= initpos; pos += 1) { // goes from 0 degrees to 180 degrees
      dosservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
}



void loop() {
unoservo.write(initpos);
dosservo.write(initpos);
tresservo.write(initpos);
cuatroservo.write(initpos);
cincoservo.write(initpos);
dedoPulgar();
dedoMenique();
dedoAnular();
dedoMedio();
dedoIndice();
}
