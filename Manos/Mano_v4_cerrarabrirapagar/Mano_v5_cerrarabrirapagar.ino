/*El código de rutina de mano, que cambia entre 4 posiciones de la mano, alternando entre los distintos dedos*/
#include <Servo.h>
#define THRESHOLD 650
#define EMG_PIN 0
Servo unoservo;  // create servo object to control a servo
Servo dosservo;
Servo tresservo;
Servo cuatroservo;
Servo cincoservo;

// twelve servo objects can be created on most boards

int uno=3;
int dos=4;
int tres=5;
int cuatro=6;
int cinco=7;

int pos =0 ;
int initpos = 180;    // variable to store the servo position
int finpos =80;

void apaga(){
  unoservo.detach();
  dosservo.detach();
  tresservo.detach();
  cuatroservo.detach();
  cincoservo.detach();
}

void cierraMano (){
  /*for (pos = initpos; pos >= finpos; pos -= 1) { // goes from 180 degrees to 0 degrees
    dosservo.write(pos);
    tresservo.write(pos);
    cuatroservo.write(pos);
    cincoservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }*/

  //unoservo.write(100);
  dosservo.write(60);
  tresservo.write(90);
  cuatroservo.write(100);
  cincoservo.write(100);
}

void inicializar()
{
  unoservo.attach(uno);  // attaches the servo on pin 9 to the servo object
  dosservo.attach(dos);  // attaches the servo on pin 9 to the servo object
  tresservo.attach(tres);  // attaches the servo on pin 9 to the servo object
  cuatroservo.attach(cuatro);  // attaches the servo on pin 9 to the servo object
  cincoservo.attach(cinco);  // attaches the servo on pin 9 to the servo object 
}

void estadoinicial()
{
  unoservo.write(initpos);
  dosservo.write(initpos);
  tresservo.write(initpos);
  cuatroservo.write(initpos);
  cincoservo.write(initpos);
}


void setup() {
  inicializar();
  Serial.begin(115200);
  estadoinicial();
  delay(1500);
  apaga();
}

 
int value; //Define la variable para printear el valor de serial
int key;
void loop() {
  /*value = analogRead(EMG_PIN);
  if(value > THRESHOLD){
   cierraMano();
   delay(10);
   estadoinicial();
  }
  Serial.println(value);
  delay(10);*/
//Codigo para abrir/
if (Serial.available()){
  key = Serial.read();
  if (key == 'c'){  
  //La mano cierra con gesto
    inicializar();
    cierraMano ();
    }
  else if (key == 'a'){ 
  //La mano abre cuando deja de hacerse el gesto
    estadoinicial();
    delay(1500);
    apaga();
    }
  }
}
